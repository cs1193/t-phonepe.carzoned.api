var express = require('express');
var bodyParser = require('body-parser');
var helmet = require('helmet');
var morgan = require('morgan');
var cors = require('cors');

var router = require('./router');

var app = express();

app.use(morgan('dev'));
app.use(helmet());
app.use(cors());

app.use(bodyParser.json({
  limit: '50mb'
}));

app.use(bodyParser.urlencoded({
  parameterLimit: 100000,
  limit: '50mb',
  extended: true
}));

app.set('SECRET_KEY', new Buffer(process.env.SECRET_KEY || 'SECRET_KEY').toString('base64'));

app.use('/', router);

module.exports = app;

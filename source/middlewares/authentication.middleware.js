function isAuthenticated (req, res, next) {
  var token = req.body.token || req.query.token || req.headers.authorization;

  if (token) {
    next();
  } else {
    res.jsonp({
      'status': 'error',
      'message': 'No token provided'
    })
  }
}

module.exports = isAuthenticated;

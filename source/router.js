var express = require('express');

var router = express.Router();

var AuthenticationRoute = require('./routes/authentication.route');
var ConfirmRideRoute = require('./routes/confirmride.route');
var RideRoute = require('./routes/rides.route');

router.get('/health-check', function (req, res) {
  res.send('OK');
});

router.use('/authenticate', AuthenticationRoute);
router.use('/confirm-ride', ConfirmRideRoute);
router.use('/rides', RideRoute);

module.exports = router;

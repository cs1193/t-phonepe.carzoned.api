var app = require('./app');

var HOST = process.env.HOST || "0.0.0.0";
var PORT = process.env.PORT || 3000;

var server = app.listen(PORT, HOST, function () {
  console.log('Server listening on ' + HOST + ':' + PORT);
});

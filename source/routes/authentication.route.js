var express = require('express');
var crypto = require('crypto');
var router = express.Router();
var isAuthenticated = require('../middlewares/authentication.middleware');

router.post('/login', function (req, res) {
  if ((req.body.email == 'user1@example.com' || req.body.email == 'user2@example.com') && req.body.password == 'password') {
    res.jsonp({
      'status': 'success',
      'token': crypto.createHash('sha256').update(req.body.email).digest('base64')
    });
  } else {
    res.jsonp({
      'status': 'error',
      'message': 'Your email/password is invalid'
    });
  }
});

router.get('/logout', isAuthenticated, function (req, res) {
  res.jsonp({
    'status': 'success',
    'message': 'User Logged Out'
  });
});

router.post('/register', function (req, res) {
  if (req.body.email === 'user1@example.com' || req.body.email === 'user2@example.com') {
    res.jsonp({
      'status': 'error',
      'message': 'User already exists'
    });
  } else {
    res.jsonp({
      'status': 'success',
      'message': 'You are a registered user.'
    });
  }
});

module.exports = router;

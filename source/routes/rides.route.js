var express = require('express');
var _ = require('lodash');
var isAuthenticated = require('../middlewares/authentication.middleware');
var crypto = require('crypto');

var router = express.Router();

router.get('/:originCoordsLat/:originCoordsLng/:destinationCoordsLat/:destinationCoordsLng/:cartype?/:seat?/:rating?/:luggagecarry?/:petcarry?', isAuthenticated, function (req, res) {
  var response = {};
  if (req.params.originCoordsLat && req.params.originCoordsLng && req.params.destinationCoordsLat && req.params.destinationCoordsLng) {
    switch (_.sample(['success', 'error'])) {
      case 'success':
        var response = {
          'status': 'success',
          'rides': generateRides(_.sample(['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']))
        };
        break;
      case 'error':
        var response = {
          'status': 'error',
          'message': 'No Rides Found'
        }
        break;
    }
  }
  res.jsonp(response);
});

function generateRides (count) {
  var rides = [];
  for (var i = 0; i < count; i++) {
    var vehicleNo = _.sample(['TN', 'AP', 'TS', 'KA', 'KL']) + " " + _.sample(['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']) + " " +  _.sample(['TN', 'AP', 'TS', 'KA', 'KL']) + " " + _.random(1000, 9999);
    var futureDate = new Date();
    var ride = {
      'id': crypto.createHash('md5').update(vehicleNo).digest("hex"),
      'vehicleNo': vehicleNo,
      'timeAway': futureDate.setMinutes(futureDate.getMinutes() +  _.sample(['3', '4', '5', '6', '7', '8', '9', '10'])),
      'seatLeft': _.sample(['1', '2', '3']),
      'ownerName': _.sample(['Ram', 'Sam', 'Dan', 'Julie', 'Jan']),
      'make': _.sample(['Hyundai', 'Nissan', 'Toyota', 'Renault', 'Ford', 'Volvo', 'Mercedes']),
      'model': _.sample(['i20', 'i10', 'Corolla', 'Camry', 'Duster', 'ALG 200', 'X90']),
      'color': _.sample(['red', 'green', 'blue', 'teal', 'chrome']),
      'carType': _.sample(['sedan', 'wagon', 'suv', 'hatchback', 'van', 'coupe', 'pickup']),
      'rating': _.sample(['1', '2', '3', '4', '5']),
      'luggageAllowed': _.sample([false, true]),
      'petAllowed': _.sample([false, true])
    };
    rides.push(ride);
  }
  return rides;
}

module.exports = router;

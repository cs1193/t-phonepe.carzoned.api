var express = require('express');
var crypto = require('crypto');
var _ = require('lodash');
var isAuthenticated = require('../middlewares/authentication.middleware');

var router = express.Router();

router.post('/', isAuthenticated, function (req, res) {
  if (req.body.rideid) {
    var response = {};
    switch(_.sample(['accepted', 'declined', 'error'])) {
      case 'accepted':
        response = {
          'status': 'success',
          'message': 'accepted',
          'data': {
            'name': 'Ram',
            'phone': _.sample(['+91 8883858838', '+91 9853453833', '+91 7824452133', '+91 7883889230', '+91 9838848929'])
          }
        };
        break;
      case 'declined':
        response = {
          'status': 'success',
          'message': 'declined'
        };
        break;
      case 'error':
        response = {
          'status': 'error',
          'message': 'There was a problem while confirming your ride.'
        };
        break;
    }
    res.jsonp(response);
  }
});

module.exports = router;
